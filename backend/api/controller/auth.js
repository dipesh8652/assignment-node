const jwt = require('jsonwebtoken')
const User = require('../models/user.js')
const bcrypt = require('bcrypt')

exports.Login = async (req, res) => {
    try {
        const { emailId, password } = req.body;
        let userData = await User.findOne({ where: { emailId: emailId } });
        if (userData) {
            let pass = await bcrypt.compare(password, userData.dataValues.password)

            if (pass) {
                let token = await jwt.sign({ emailId: userData.dataValues.emailId,roleId:userData.dataValues.roleId },'key');
                await User.update({ token: token }, { where: { emailId: userData.dataValues.emailId } })
                res.status(200).send({ token })

            } else {
                res.status(404).send({ message: 'Invalid Password' })
            }
        }
        else {
            res.send(" User not found");
        }
    } catch (err) {
        res.send(err)
    }

}