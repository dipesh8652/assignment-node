const multer = require('multer');
xlsxtojson = require('xlsx-to-json');

exceltojson = xlsxtojson;

var storage = multer.diskStorage({
    destination: function(req, file,cb){
        cb(null,"./upload/bulkupload");  
    },
    filename:  function( req, file, cb){
        cb(null,file.originalname);
    }
});

const upload = multer({storage}).single('bulkupload');

exports.bulkupload = async (req,res,next)=>{
    upload(req,res,async (err)=>{
        if(!upload){
            res.status(400).json({message:'error while file uploading'});
        }
        else{
            exceltojson({
                input: req.file.path,
                output:"upload/bulkupload/"+Date.now()+".json",
                lowerCaseHeaders:true
            },function(err,result){
                if(err){
                    res.json(err);
                }
                else{
                    res.json(result);
                }
            
            });
        }
    })
}




