const Cart = require('../models/cart');
const Product = require('../models/product');


//Cart.hasMany(Product, { foreignKey: 'categoryId' });




exports.getCart = async (req, res) => {
    try {
        let getCart = await Cart.findAll()
        if (getCart.length === 0) {
            res.status(404).send({ error: 'data not found' });
            console.log(error)
        }
        else {
            res.status(200).send(getCart)
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}



exports.AddToCart = async (req,res)=>{
    try{
    // let productExist = await Cart.findOne({
    //     where: { Id, productId }
    //   });
    //   if (productExist) {
    //     res.status(409).json({ mesage: "Product already exist in the cart!" });
    //   } else{
    //     res.status(200).send(getProducts)
    // }
    const {Id,categoryId,productId,quantity} = req.body
    let createdCart = await Cart.create({Id,categoryId,productId,quantity})
    if(!createdCart){
        res.status(422).send({message: 'cart not created'});
    }else{
        res.status(200).send({message:'cart added', createdCart});
    }
    }
    catch(err){
        res.status(500).send(err);
    }
}

// exports.AddToCart = async (req, res) => {
//     try 
//     {
//         const Id = req.params.id;
//         const productId = req.body;
//         let productExist = await Cart.findOne({ where: { Id, productId } });
//         if (productExist) 
//         {
//             res.status(409).json({ mesage: "Product already exist in the cart!" });
//         }
//         else 
//         {
//             const { categoryId, quantity } = req.body
//             let createdCart = await Cart.create({ Id, categoryId, productId, quantity })
//             if (!createdCart) 
//             {
//                 res.status(422).send({ message: 'cart not created' });
//             } else 
//             {
//                 res.status(200).send({ message: 'cart added', createdCart });
//             }

//         }
//     }
//     catch (err)
//      {
//         res.status(500).send(err);
//     }
// }


exports.deleteCart = async (req, res) => {
    try {
        const { Id } = req.body
        let deleteCartId = await Cart.destroy({ where: { Id: Id } })
        if (deleteCartId) {
            res.status(200).send({ message: 'record deleted' });
        }
        else {
            res.status(404).send({ error: 'data not found' });
          
        }
    }
    catch (error) {
        res.status(500).send(error);
    }

}

exports.updateCart = async (req, res) => {
    try {
        const {quantity,Id}  = req.body
        let updateCart = await Cart.update({quantity}, { where: { Id: Id} })
        if (updateCart) {
            res.status(200).send({ message: 'cart updated' });
        }
        else {
           
            res.status(404).send({ error: 'cart not found' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}