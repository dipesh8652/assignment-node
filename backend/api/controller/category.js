const Category = require('../models/category')
const Product = require('../models/product')

Product.belongsTo(Category, { foreignKey: 'categoryId' })

exports.getCategory = async (req, res) => {
    try {
        let getCategory = await Category.findAll()
        if (getCategory.length === 0) {
            res.status(404).send({ error: 'data not found' });
        }
        else {
            res.status(200).send(getCategory)
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}


exports.getProductsByCategory = async (req, res) => {
    try {
        const { categoryId } = req.body;
        let getProductsByCategory = await Category.findAll({ where: { categoryId: categoryId } });
        if (getProductsByCategory.length === 0) {
            res.status(404).send({ err: 'data not found' });
            console.log(err);
        }
        else {
            res.status(200).send(getProductsByCategory);
        }
    }
    catch (err) {
        res.status(500).send(err);
    }

}


exports.AddCategory = async (req, res) => {
    try {
        const { categoryName, createdBy } = req.body
        let addCategory = await Category.create({ categoryName, createdBy })
        if (addCategory) {
            res.status(201).json({ message: "Category Added", data: addCategory })
          
        }
        else {
            res.status(422).send({ error: 'Category not added' });
        }
    }
    catch (error) {
        console.log(error)
        res.status(500).send(error);
    }
}

exports.updateCategory = async (req, res) => {
    try {
        const { categoryId, categoryName,createdBy } = req.body
        let updateCategory = await Category.update({ categoryName,createdBy}, { where: { categoryId:categoryId} })
        if (updateCategory) {
            res.status(200).send({ message: 'category updated' });
        }
        else {
           
            res.status(404).send({ error: 'category not found' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

exports.deleteCategory = async (req, res) => {
    try {
        const { categoryId } = req.body
        let deleteCategory = await Category.destroy({ where: { categoryId:categoryId } })
        if (deleteCategory) {
            res.status(200).send({ message: 'record deleted' });
        }
        else {
            res.status(404).send({ error: 'data not found' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}
exports.pagination = async (req, res) => {
    try {
        const size = 3;
        const page = req.params.page;
        const limit = size;
        const offset = (page - 1) * size;
        let getdata = await Category.findAll({ offset, limit });
        if (getdata.length !== 0) {
            res.status(200).send(getdata)
        }
        else {
            res.status(400).send({ err: 'data not found' });
        }
    }
    catch (err) {
        res.status(500).send(err)
    }
}


