const Product = require('../models/product')
const Category = require('../models/category')
const Sequelize = require('sequelize');
const Op = Sequelize.Op


Category.hasMany(Product, { foreignKey: 'categoryId' })

exports.getProducts = async (req, res) => {
    try {
        let getProducts = await Product.findAll()
        if (getProducts.length === 0) {
            res.status(404).send({ error: 'data not found' });
            console.log(error)
        }
        else {
            res.status(200).send(getProducts)
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

exports.AddProducts = async (req, res) => {
    try {
        const { productId, productName, price, productImage, categoryName, categoryId, createdBy } = req.body
        let addProducts = await Product.create({ productId, productName, price, productImage, categoryName, categoryId, createdBy })
        if (addProducts) {
            res.status(201).json({ message: "Product Added", data: addProducts })
        }
        else {
            res.status(404).send({ error: 'data not created' });
            console.log(error)
           
        }

    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
}

exports.updateProducts = async (req, res) => {
    try {
        const { productId, productName, price, categoryName, categoryId, createdBy } = req.body
        let updateProduct = await Product.update({ productName, price, categoryName, categoryId, createdBy }, { where: { productId: productId } })
        if (updateProduct) {
            res.status(200).send({ message: 'product updated' });
        }
        else {
            res.status(404).send({ error: 'product not found' });
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

exports.deleteProductsById = async (req, res) => {
    try {
        const { productId } = req.body
        let deleteProductsById = await Product.destroy({ where: { productId: productId } })
        if (deleteProductsById) {
            res.status(200).send({ message: 'record deleted' });
        }
        else {
            res.status(404).send({ error: 'data not found' });
          
        }
    }
    catch (error) {
        res.status(500).send(error);
    }

}

exports.pagination = async (req, res) => {
    try {
        const size = 3;
        const page = req.params.page;
        const limit = size;
        const offset = (page - 1) * size;
        let getdata = await Product.findAll({ offset, limit });
        if (getdata.length !== 0) {
            res.status(200).send(getdata)
        }
        else {
            res.status(400).send({ err: 'data not found' });
        }
    }
    catch (err) {
        res.status(500).send(err)
    }
}

//getiing product in between
exports.getProductsByPrice = async (req, res) => {
    try {

        const { priceFrom, priceTo } = req.body
        let getProducts = await Product.findAll({ where: { price: { [Op.between]: [priceFrom, priceTo] } } });
        if (getProducts.length === 0) {
            res.status(404).send({ err: 'data not found' });
            console.log(err);
        }
        else {
            res.status(200).send(getProducts);
        }
    }
    catch (err) {
        res.status(500).send(err);
    }
}

//get product by categoty 
exports.getProductsByCategory = async (req, res) => {
    try {
        const { categoryId } = req.body;
        let getProductsByCategory = await Product.findAll({ where: { categoryId: categoryId } });
        if (getProductsByCategory.length === 0) {
            res.status(404).send({ err: 'data not found' });
            console.log(err);
        }
        else {
            res.status(200).send(getProductsByCategory);
        }
    }
    catch (err) {
        res.status(500).send(err);
    }

}