const Roles = require('../models/roles.js')

exports.AddRoles = async (req, res) => {
    try {
        const { roleName } = req.body
        let AddRole = await Roles.create({ roleName: roleName })
        if (!AddRole) {
            res.status(422).send({ error: 'Role not created' });
        }
        else {
            res.status(201).json({ message: "Role Added", data: AddRole })
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
}
