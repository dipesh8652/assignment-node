const multer = require('multer');


var storage = multer.diskStorage({
    destination: function(req, file,cb){
        cb(null,"./upload/images");  
    },
    filename:  function( req, file, cb){
        cb(null,file.originalname);
    }
});

const upload = multer({storage:storage}).single("demo_image");
const uploads = multer({storage:storage}).array('mul_images',5)

exports.fileupload = (req,res) =>{
    upload(req,res,(err)=>{
        if(err){
            res.status(400).send('somthing went wrong');
        }
        res.send(req.file);
    });
} 


exports.postfileupload = (req,res) =>{
    uploads(req,res,(err)=>{
        if(err){
            res.status(400).send('somthing went wrong');
        }
        res.send(req.files);
    });
} 


// var stream = require('stream');
 
// const db = require('../../config/database');
// const File = db.files;
 
// exports.uploadFile = (req, res) => {
//   File.create({
//     type: req.file.mimetype,
//     name: req.file.originalname,
//     data: req.file.buffer
//   }).then(() => {
//     res.json({msg:'File uploaded successfully! -> filename = ' + req.file.originalname});
//   }).catch(err => {
//     console.log(err);
//     res.json({msg: 'Error', detail: err});
//   });
// }
 
// exports.listAllFiles = (req, res) => {
//   File.findAll({attributes: ['id', 'name']}).then(files => {
//     res.json(files);
//   }).catch(err => {
//     console.log(err);
//     res.json({msg: 'Error', detail: err});
//   });
// }
 
// exports.downloadFile = (req, res) => {
//   File.findById(req.params.id).then(file => {
//     var fileContents = Buffer.from(file.data, "base64");
//     var readStream = new stream.PassThrough();
//     readStream.end(fileContents);
    
//     res.set('Content-disposition', 'attachment; filename=' + file.name);
//     res.set('Content-Type', file.type);
 
//     readStream.pipe(res);
//   }).catch(err => {
//     console.log(err);
//     res.json({msg: 'Error', detail: err});
//   });
// }