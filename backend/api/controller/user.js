const User = require('../models/user.js')
const bcrypt = require('bcrypt')


exports.getUser = async (req, res) => {
    try {
        let getUser = await User.findAll()
        if (getUser.length === 0) {
            res.status(404).send({ error: 'data not found' });
        }
        else {
            res.status(200).send(getUser)
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
}

exports.AddUser = async (req, res) => {
    try {
        const { userName,emailId,number, password,roleId } = req.body
        let AddUser = await User.create({ userName,emailId,number, password,roleId })
        if (AddUser) {
            res.status(201).json({ message: "User Added", data: AddUser })
         
        }
        else {
            res.status(422).send({ error: 'User not created' });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
}

exports.updateUser = async (req, res) => {
    try {
        const { emailId, oldPassword, newPassword } = req.body
        let userData = await User.findOne({ where: { emailId:emailId } })

        if (!userData) {
            res.status(404).send({ error: 'User not found' });
        }
        else {
            let checkPassword = await bcrypt.compare(oldPassword, userData.dataValues.password)
            if (!checkPassword) {
                res.status(422).send({ error: 'invalid password' });
            }
            else {
                let updatePassword = await userData.update({ password: newPassword }, { where: { emailId: userData.dataValues.emailId } })
                if (!updatePassword) {
                    res.status(422).send({ error: 'Password not updated' });
                }
                else {
                    res.status(201).send("Password Updated")
                }
            }

        }
    }
    catch (error) {
        console.log(error)
        res.status(500).send(error);
    }
}