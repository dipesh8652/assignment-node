const jwt = require('jsonwebtoken')
const User = require('../models/user.js')




exports.admin = async (req, res, next) => {

    try {
        //get auth header value
        const bearerheader = req.headers['authorization'];

        //check bearer is undefined

        //slpit the space
        const bearer = bearerheader.split(' ');
        //get token from array
        const bearertoken = bearer[1];
        //set the token
        let decode = jwt.decode(bearertoken, 'key');
        let userData = await User.findOne({ where: { emailId: decode.emailId, roleId: decode.roleId, token: bearertoken } });
        if (userData.roleId == 1) {
            next();
        }
        else {
            res.status(401).send({ message: 'unautorized user' })
        }
    }
    catch (err) {
        res.status(403).send({ message: 'unauthorized user ' });

    }

}


exports.super = async (req, res, next) => {
    try {
        //get auth header value
        const bearerheader = req.headers['authorization'];

        //check bearer is undefined

        //slpit the space
        const bearer = bearerheader.split(' ');
        //get token from array
        const bearertoken = bearer[1];
        //set the token
        let decode = jwt.decode(bearertoken, 'key');
        let userData = await User.findOne({ where: { emailId: decode.emailId, roleId: decode.roleId, token: bearertoken } });

        if (userData.roleId == 2) {
            next()
        } else {
            res.status(401).send({ message: 'unautorized user' })
        }
    } catch (err) {
        res.status(401).send({ message: 'unautorized user' })

    }

}

