const Sequelize = require('sequelize')
const sequelize = require('../../config/database')



const Cart = sequelize.define('cart',{
    Id:{
        type: Sequelize.INTEGER,
         primaryKey: true,
         allowNull: false,
          autoIncrement: true
    },
    productId:{
        type:Sequelize.INTEGER
    },
    categoryId: {
        type: Sequelize.INTEGER
    },
    quantity:{
        type:Sequelize.INTEGER
    }
},

{
    freezeTableName: true,
    tableName: 'cart'
});

Cart.associate = function (models) {
    Cart.belongsTo(models.product, { foreignKey: 'productId', as: 'product' });
    Cart.belongsTo(models.category, { foreignKey: 'categoryId', as: 'category' });
}




module.exports = Cart;