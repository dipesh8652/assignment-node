const Category = require('./category.js')
const Sequelize = require('sequelize')
const sequelize = require('../../config/database')

//product table
const Product = sequelize.define('product', {
    productId: {
         type: Sequelize.INTEGER,
          primaryKey: true,
           allowNull: false,
            autoIncrement: true
         },

    productName: {
         type: Sequelize.STRING,
          unique: true, 
          allowNull: false
         },
    
    price : {
         type: Sequelize.INTEGER,
          allowNull: false 
        },

     productImage : {
          type: Sequelize.TEXT 
        },

    categoryName : {
         type: Sequelize.STRING,
          allowNull: false 
        },

    categoryId : {
         type: Sequelize.INTEGER,
          allowNull: false
         },

    createdBy : {
         type: Sequelize.STRING,
          allowNull: false 
        }
},
    { freezeTableName: true, tableName: 'product' })


    Product.associate = function (models) {
        Product.belongsTo(models.category, { foreignKey: 'categoryId', as: 'category' });
    }

module.exports = Product