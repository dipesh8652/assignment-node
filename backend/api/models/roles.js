const Sequelize = require('sequelize')
const sequelize = require('../../config/database')

//roles table
const Roles = sequelize.define('roles', {
    roleId: { 
        type: Sequelize.INTEGER,
         primaryKey: true,
          allowNull: false,
          autoIncrement: true
         },

    roleName: { 
        type: Sequelize.STRING,
        unique:true,
         allowNull: false
         }

},
    { freezeTableName: true, tableName: 'roles' })


module.exports = Roles