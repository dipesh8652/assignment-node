const Sequelize= require('sequelize');
const sequelize = require('../../config/database');



const Upload = sequelize.define('file',{
    
         type: {
            type: Sequelize.STRING
          },
          name: {
            type: Sequelize.STRING
          },
          data: {
            type: Sequelize.BLOB('long')
          }
},
{ freezeTableName: true, tableName: 'file' })

module.exports = Upload;
