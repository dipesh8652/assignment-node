const express = require('express');
const router = express.Router();

const cart = require('../controller/cart');


router.get('/get-cart',cart.getCart);
router.post('/add-cart',cart.AddToCart);
router.delete('/delete-cart',cart.deleteCart);
router.put('/update-cart',cart.updateCart)





module.exports=router;