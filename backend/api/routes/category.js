const express = require('express');
const router = express.Router();

const verify = require('../middleware/checkauth');

const category=require('../controller/category');


router.get('/get-category',category.getCategory);
router.get('/get-category-id', category.getProductsByCategory);
router.get('/category-page/:page',category.pagination );
router.post('/add-category' ,verify.admin,category.AddCategory);
router.put('/update-category',verify.admin,category.updateCategory);
router.delete('/delete-category',verify.admin,category.deleteCategory);

module.exports = router;

