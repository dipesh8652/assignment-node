const express = require('express');
var router = express.Router();

const productRouter = require('./product');
const categoryRouter = require('./category');
const userRouter=require('./user');
const auth=require('./auth');
const rolesRoute=require('./roles');
const uploadRoute=require('./upload');
const bulkuploadRoute = require('./bulkupload');
const cart = require('./cart');

router.use('/auth', auth);
router.use('/product', productRouter);
router.use('/category', categoryRouter);
router.use('/user', userRouter);
router.use('/roles', rolesRoute);
router.use('/upload',uploadRoute);
router.use('/bulkupload',bulkuploadRoute);
router.use('/cart',cart);

module.exports = router;