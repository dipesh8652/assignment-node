const express = require('express');
const router = express.Router();

const product=require('../controller/product');

const verify = require('../middleware/checkauth');

router.get('/get-products', product.getProducts);
router.get('/product-page/:page',product.pagination);
router.get('/get-by-price',product.getProductsByPrice);
router.get('/category',product.getProductsByCategory);
router.post('/add-products',verify.super,product.AddProducts);
router.put('/update-products',verify.super,product.updateProducts);
router.delete('/delete-products',verify.super, product.deleteProductsById);

module.exports=router;

