const express = require('express');
const router = express.Router();
const roles = require('../controller/roles.js');

router.post('/add-roles', roles.AddRoles);

module.exports = router;