const express = require('express');
const router = express.Router();
const user = require('../controller/user.js');



router.get('/get-user',user.getUser);
router.post('/add-user', user.AddUser);
router.put('/update-user', user.updateUser);

module.exports = router;