'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
   queryInterface.renameColumn('product','productImage','image');
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.renameColumn('product','image','productImage');

  }
};
