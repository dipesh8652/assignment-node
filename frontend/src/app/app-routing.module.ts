import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './view/home/home.component';
import { RegisterComponent } from './view/register/register.component';
import { LoginComponent } from './view/login/login.component';


const routes: Routes = [
  { path: '', 
loadChildren: ()=>import('./view/view.module').then(m=>m.ViewModule)
},

];

// const routes:Routes=[{
//   path:"",
//   component:HomeComponent
// },
// {
//   path:'register',
//   component:RegisterComponent
// },
// {
//   path:'login',
//   component:LoginComponent
// }
// ]



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
