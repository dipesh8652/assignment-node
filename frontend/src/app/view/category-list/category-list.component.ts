import { Component, OnInit } from '@angular/core';
import { CategoryService } from "../category/category.service";       
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { CategoryComponent } from '../category/category.component';
@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  listData: any = [];

  dataSource: MatTableDataSource<CategoryService>;
  displayedColumns: string[] = ['categoryId','categoryName', 'createdBy'];
  

  constructor(private http:CategoryService,
      private dialog:MatDialog) { }

  ngOnInit(): void {
   this.getCategoryData();
   
  }
  getCategoryData(){
    this.http.getCategoryData().subscribe(data => {
      this.listData = data;
      this.dataSource = new MatTableDataSource<CategoryService>(this.listData);
    })
   
  }

 
  deleteCategory(categoryId: number) {
    this.http.deleteCategory(categoryId)
      .subscribe(
        data => {
          console.log(data);
          this.getCategoryData();
        },
        error => console.log(error));
  }

}
