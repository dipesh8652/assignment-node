import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators,FormControl } from "@angular/forms";
import { CategoryModel } from '../models/category.model';
import { CategoryService } from "./category.service";       
import { MatTableDataSource } from "@angular/material/table";



@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {



  category: CategoryModel = new CategoryModel();
  addcategory: FormGroup;
  constructor(private http:CategoryService) { }

  ngOnInit(): void {
    this.addcategory = new FormGroup({
      categoryId:new FormControl(''),
      categoryName: new FormControl(''),
      createdBy: new FormControl(''),
     });
  }

  onAddCategory(){
    // console.log(this.addcategory.value);
    this.http.postCategoryData(this.addcategory.value).subscribe((res)=>{
     console.log('response from post data is ', res);
   
   },(err)=>{
     console.log('error during post data is ', err)
   })
  }
  
  
  // isControlHasError(controlName: string, validationType: string): boolean {
  //   const control = this.addcategory.controls[controlName];
  //   if (!control) {
  //     return false;
  //   }
  //   const result = control.hasError(validationType) && (control.dirty || control.touched);
  //   return result;
  // }
}