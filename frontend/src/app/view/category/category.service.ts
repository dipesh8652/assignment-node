import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap, map } from 'rxjs/operators';



const apiUrl = 'http://localhost:5000/category'


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http:HttpClient) { }                                                                       

  postCategoryData(data){
    return this.http.post(apiUrl+'/add-category',data);
  }
  getCategoryData():Observable<Object>{
    return this.http.get(apiUrl+'/get-category')

  }
  deleteCategory(categoryId: number): Observable<any> {
    return this.http.delete(`${apiUrl+'/delete-category'}/${categoryId}`);
  }

}
