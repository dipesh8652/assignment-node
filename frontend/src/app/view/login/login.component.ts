import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../models/login.model';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { LoginService } from "../login/login.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: LoginModel = new LoginModel();
  loginForm: FormGroup;
  hide = true;

  constructor(private formBuilder: FormBuilder,private http:LoginService) { }

  ngOnInit() {
  //   this.loginForm = this.formBuilder.group({
  //     'emailId': [this.user.emailId, [
  //       Validators.required,
  //       Validators.email
  //     ]],
  //     'password': [this.user.password, [
  //       Validators.required,
  //       Validators.minLength(6),
  //       Validators.maxLength(30)
  //     ]]
  //   });

  this.loginForm = new FormGroup({
    emailId: new FormControl('',[Validators.required,Validators.email]),
    password: new FormControl('',[Validators.required,Validators.minLength(6),Validators.maxLength(30)]),


   });
  }
  

        onLoginSubmit() {
    //alert(this.user.emailId + ' ' + this.user.password);
    this.http.postLoginData(this.loginForm.value).subscribe((res)=>{
        console.log('response from post data is ', res);
       },(err)=>{
         console.log('error during post data is ', err)
       })
      
        }
        isControlHasError(controlName: string, validationType: string): boolean {
          const control = this.loginForm.controls[controlName];
          if (!control) {
            return false;
          }
          const result = control.hasError(validationType) && (control.dirty || control.touched);
          return result;
        }
  }


  


