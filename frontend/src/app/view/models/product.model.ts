export class ProductModel {
    productId:number;
    productName: String;
    categoryName: String;
    productImage:String;
    createdBy:String;
    price:Number;
    categoryId:Number;
}