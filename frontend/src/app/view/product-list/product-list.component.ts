import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { ProductService } from "../product/product.service";
import { ProductModel } from "../models/product.model";
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  listData: any = [];

  dataSource: MatTableDataSource<ProductService>;
  displayedColumns: string[] = ['productId','productName','productImage' ,'price','categoryName','actions'];

  constructor(private  http:ProductService) { }



  ngOnInit(): void {
    this.getProductData();
  }

getProductData(){
  this.http.getProductData().subscribe(data => {
    this.listData = data;
    this.dataSource = new MatTableDataSource<ProductService>(this.listData);
  })
}

  deleteProduct(productId: number) {
    this.http.deleteProduct(productId)
      .subscribe(
        data => {
          console.log(data);
          this.getProductData();
        },
        error => console.log(error));
  }


}
