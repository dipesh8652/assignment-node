import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators,FormControl } from "@angular/forms";
import { ProductModel } from '../models/product.model';
import { ProductService } from "./product.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {


  product: ProductModel = new ProductModel();
  addproduct: FormGroup;
 

  constructor(private http:ProductService) { }
  
  ngOnInit(): void {
    this.addproduct = new FormGroup({
      productName: new FormControl('',Validators.required),
      categoryName: new FormControl('',Validators.required),
      productImage: new FormControl('',Validators.required),
      createdBy: new FormControl(''),
      price : new FormControl('',Validators.required),
      categoryId:new FormControl('',Validators.required)
     });
  
  }

 onAddProduct(){
   console.log(this.addproduct.value);
   this.http.postProductData(this.addproduct.value).subscribe((res)=>{
    console.log('response from post data is ', res);
  
  },(err)=>{
    console.log('error during post data is ', err)
  })
   
 }
 onFileSelected(event){
    console.log(event);
 }
 isControlHasError(controlName: string, validationType: string): boolean {
  const control = this.addproduct.controls[controlName];
  if (!control) {
    return false;
  }
  const result = control.hasError(validationType) && (control.dirty || control.touched);
  return result;
}


}
