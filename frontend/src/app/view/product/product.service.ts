import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";


const apiUrl = 'http://localhost:5000/product'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }                                                                       

  postProductData(data){
    return this.http.post(apiUrl+'/add-products',data);
  }
  getProductData():Observable<Object>{
    return this.http.get(apiUrl+'/get-products')

  }
  deleteProduct(productId:number): Observable<any> {
    return this.http.delete(`${apiUrl}/delete-products/${productId}`, { responseType: 'text' })
    
   //return this.http.delete(apiUrl+'/delete-products/'+productId);
  }

  editProduct(productId:number,data){
    const url =`${apiUrl}/update-products/${productId}`;
    return this.http.put(url,data)
  }
}
