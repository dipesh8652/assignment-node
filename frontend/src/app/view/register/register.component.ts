import { Component, OnInit } from '@angular/core';
import { RegisterModel } from '../models/register.model';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { RegisterService } from "../register/register.service";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  
  user: RegisterModel = new RegisterModel();
  registerForm: FormGroup;
  hide = true;
  alert:boolean=false;  

  constructor(private formBuilder: FormBuilder,private http: RegisterService ) { }

  ngOnInit() {
//     this.registerForm = this.formBuilder.group({
//       'userName': [this.user.userName, [
//         Validators.required
//       ]],
//       'emailId': [this.user.emailId, [
//         Validators.required,
//         Validators.email
//       ]],
//       'password': [this.user.password, [
//         Validators.required,
//         Validators.minLength(6),
//         Validators.maxLength(30)
//       ]],

//       'number' : [this.user.number,[
//         Validators.required
//       ]],
//       'roleId' : [this.user.roleId
// ]
//     });

  
//   }
this.registerForm = new FormGroup({
  userName: new FormControl('',[Validators.required]),
  emailId: new FormControl('',[Validators.required,Validators.email]),
  password: new FormControl('',[Validators.required,Validators.minLength(6),Validators.maxLength(30)]),
  number: new FormControl('',[Validators.required]),
  roleId : new FormControl('')
 });
}


  onRegisterSubmit() {
      this.http.postAPIData(this.registerForm.value).subscribe((res)=>{
        console.log('response from post data is ', res);
        this.alert=true;
      },(err)=>{
        console.log('error during post data is ', err)
      })
      
      
      this.registerForm.reset({})
    
    }
    closealert(){
      this.alert=false;
    }
    isControlHasError(controlName: string, validationType: string): boolean {
      const control = this.registerForm.controls[controlName];
      if (!control) {
        return false;
      }
      const result = control.hasError(validationType) && (control.dirty || control.touched);
      return result;
    }
  }

