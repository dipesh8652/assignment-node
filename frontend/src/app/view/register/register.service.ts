import { Injectable } from '@angular/core';
import { HttpClient  } from "@angular/common/http";
//import { catchError, tap, map } from 'rxjs/operators';



const apiUrl = "http://localhost:5000/user/add-user";

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor( private http:HttpClient) { }

  postAPIData(data){
    return this.http.post(apiUrl,data)
  }
}
