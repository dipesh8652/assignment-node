import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ViewComponent } from './view.component';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { CategoryListComponent } from "./category-list/category-list.component";
import { ProductListComponent } from './product-list/product-list.component';



const routes:Routes=[
{
  path:'',
  component:ViewComponent,
children:[
  {
    path:'',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path:"home",
    component:HomeComponent
  },
  {
    path:'register',
    component:RegisterComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'product',
    component:ProductComponent
  },
  {
    path:'category',
    component:CategoryComponent
  },
  {
    path:'category-list',
    component:CategoryListComponent
  },
  {
    path:'product-list',
    component:ProductListComponent
  }

]
  

}
]



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRouting { }
