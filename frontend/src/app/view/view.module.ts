import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavBarComponent } from "./nav-bar/nav-bar.component";
import { ViewRouting } from "./view-routing.module";
import { ViewComponent } from './view.component';
import { MaterialModule } from "../material.module";
import { ReactiveFormsModule,FormsModule } from "@angular/forms";
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { ProductListComponent } from './product-list/product-list.component';

@NgModule({
  declarations: [HomeComponent,LoginComponent,RegisterComponent, ViewComponent,NavBarComponent, ProductComponent, CategoryComponent, CategoryListComponent, ProductListComponent],
  imports: [
    CommonModule,
    ViewRouting,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
   
  ],
})
export class ViewModule { 
  constructor(){
    console.log('view model loaded ');
    
  }
}
